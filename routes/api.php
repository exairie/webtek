<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');

Route::group(['middleware' => ['api_auth']], function () {
    Route::get('/whoami', 'AuthController@who');
    Route::get('/logout', 'AuthController@logout');
    Route::post('/board', 'BoardController@newboard');
    Route::put('/board/{board}', 'BoardController@update');
    Route::delete('/board/{board}', 'BoardController@delete');
    Route::get('/board', 'BoardController@all');
    Route::get('/board/{board}', 'BoardController@pick');
    Route::post('/board/{board}/member', 'BoardController@addmember');
    Route::delete('/board/{board}/member/{user}', 'BoardController@deletemember');

    Route::post('/board/{board}/list', "ListController@create");
    Route::put('/board/{board}/list/{list}', "ListController@update");
    Route::delete('/board/{board}/list/{list}', "ListController@delete");
    Route::post('/board/{board}/list/{list}/right', "ListController@moveright");
    Route::post('/board/{board}/list/{list}/left', "ListController@moveleft");

    Route::post('/board/{board}/list/{list}/card', 'CardController@create');
    Route::put('/board/{board}/list/{list}/card/{card}', 'CardController@update');
    Route::delete('/board/{board}/list/{list}/card/{card}', 'CardController@delete');
    Route::post('/card/{card}/up', 'CardController@moveup');
    Route::post('/card/{card}/down', 'CardController@movedown');
    Route::post('/card/{card}/move/{list}', 'CardController@movetolist');
});
