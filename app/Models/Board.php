<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $creator_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Board extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['creator_id', 'name', 'created_at', 'updated_at'];

}
