<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $board_id
 * @property int $list_order
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class BoardList extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['board_id', 'list_order', 'name', 'created_at', 'updated_at'];

}
