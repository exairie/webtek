<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $list_id
 * @property int $list_order
 * @property string $task
 * @property string $created_at
 * @property string $updated_at
 */
class Card extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['list_id', 'list_order', 'task', 'created_at', 'updated_at'];

}
