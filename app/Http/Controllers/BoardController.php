<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\User;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    public function newboard(Request $r)
    { }
    public function update(Request $r, Board $board)
    { }
    public function delete(Request $r, Board $board)
    { }
    public function all(Request $r)
    { }
    public function pick(Request $r, Board $board)
    { }
    public function addmember(Request $r, Board $board)
    { }
    public function deletemember(Request $r, Board $board, User $user)
    { }
}
