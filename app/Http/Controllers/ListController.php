<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\BoardList;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function create(Request $r, Board $board)
    { }
    public function update(Request $r, Board $board, BoardList $list)
    { }
    public function delete(Request $r, Board $board, BoardList $list)
    { }
    public function moveright(Request $r, Board $board, BoardList $list)
    { }
    public function moveleft(Request $r, Board $board, BoardList $list)
    { }
}
