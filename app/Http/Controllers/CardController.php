<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\BoardList;
use App\Models\Card;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function create(Request $r, Board $board, BoardList $list)
    { }
    public function update(Request $r, Board $board, BoardList $list, Card $card)
    { }
    public function delete(Request $r, Board $board, BoardList $list, Card $card)
    { }
    public function moveup(Request $r, Card $card)
    { }
    public function movedown(Request $r, Card $card)
    { }
    public function movetolist(Request $r, Card $card, BoardList $list)
    { }
}
