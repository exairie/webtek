<?php

namespace App\Http\Controllers;

use App\Models\LoginToken;
use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function who(Request $r)
    {
        return response()->json($r->userdata);
    }
    public function register(Request $r)
    {
        // TODO : Validate
        $r->validate([
            'first_name' => 'alpha|min:2|max:20',
            'last_name' => 'alpha|min:2|max:20',
            'username' => 'alpha|min:5|max:12|unique:users',
        ]);
        $newUser = User::create([
            'first_name' => $r->input('first_name'),
            'last_name' => $r->input('last_name'),
            'username' => $r->input('username'),
            'password' => $r->input('password')
        ]);
        return response()->json([
            'token' => $this->createToken($newUser)
        ]);
    }
    private function createToken(User $user)
    {
        $tokendata = [
            'id' => $user->id
        ];
        $token = encrypt($tokendata);

        LoginToken::create([
            'user_id' => $user->id,
            'token' => $token
        ]);

        return $token;
    }
    private function _login($username, $password)
    {
        $checkUser = User::where('username', $username)->where('password', $password)->first();
        if ($checkUser) {
            $this->createToken($checkUser);
        } else {
            return null;
        }
    }
    public function login(Request $r)
    {
        $username = $r->input('username');
        $password = $r->input('password');

        return $this->_login($username, $password);
    }
    public function logout(Request $r)
    {
        if ($r->userdata != null) {
            LoginToken::where('user_id', $r->userdata->id)->delete();
            return response()->json([]);
        }
    }
}
