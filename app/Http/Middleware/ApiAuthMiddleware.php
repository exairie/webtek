<?php

namespace App\Http\Middleware;

use App\Models\LoginToken;
use Closure;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->input('token');

        if (!$token) {
            return response()->json(['error' => 'No Auth Token'], 503);
        }

        $checktoken = LoginToken::where('token', $token)->with(['user'])->first();
        if (!$checktoken) {
            return response()->json(['error' => 'Invalid Token'], 503);
        }

        if (!$checktoken->user) {
            return response()->json(['error' => 'Invalid User Data'], 500);
        }

        $request->userdata = $checktoken->user;

        return $next($request);
    }
}
